import Head from 'next/head'
import { useState } from 'react'

export default function Home() {

  const [count, useCount] = useState(1);
	const [otherCount, updateOtherCount] = useState(1);

  return (
    <div>
      <h1>Teach Ryan</h1>

      <h2>{count}</h2>
      <button onClick={() => useCount(count + 1)}>click me</button>

      <h2>{otherCount}</h2>
      <button onClick={() => updateOtherCount(otherCount * 2)}>click me</button>
    </div>
  )
}
